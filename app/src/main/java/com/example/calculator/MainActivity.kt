package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.widget.ButtonBarLayout
import kotlinx.android.synthetic.main.activity_main.*
import java.nio.file.attribute.AclEntry

class MainActivity : AppCompatActivity(),View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        deleteAll()
    }

    private var firstNumber = 0.0
    private var secondNumber = 0.0
    private var operation = ""

    private fun init() {
        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        val button = v as Button
        resultTextView.text = resultTextView.text.toString() + button.text.toString()
    }

    fun delete(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            resultTextView.text = value.substring(0, value.length - 1)
        }
    }
    private fun deleteAll() {
        buttonDelete.setOnLongClickListener {
            resultTextView.text = ""
            true
        }
    }

    fun multiply(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            operation = "x"
            firstNumber = value.toDouble()
            resultTextView.text = ""
        }
    }

    fun divide(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            operation = ":"
            firstNumber = value.toDouble()
            resultTextView.text = ""
        }
    }

    fun plus(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            operation = "+"
            firstNumber = value.toDouble()
            resultTextView.text = ""
        }
    }

    fun minus(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            operation = "-"
            firstNumber = value.toDouble()
            resultTextView.text = ""
        }
    }

    fun float(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty() && ("." in value) == false) {
            resultTextView.text = resultTextView.text.toString() + "."
        }
    }

    fun equal(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            secondNumber = value.toDouble()
            if (operation == "x") {
                resultTextView.text = (firstNumber*secondNumber).toString()
            }else if (operation == ":") {
                resultTextView.text = (firstNumber/secondNumber).toString()
            }else if (operation == "+") {
                resultTextView.text = (firstNumber+secondNumber).toString()
            }else if (operation == "-") {
                resultTextView.text = (firstNumber-secondNumber).toString()
            }
        }
        firstNumber = 0.0
        secondNumber = 0.0
        operation = ""
    }
}